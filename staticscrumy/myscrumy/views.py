from django.contrib.auth.models import User, Group
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, render_to_response
from django.forms.models import model_to_dict
from django.http import HttpResponseRedirect, HttpResponse
from .forms import TaskPostForm, AddUserForm, MoveTaskForm, AssignTaskOwnerForm
from .models import ScrumyUser, ScrumyGoals, Status, ScrumyHistory
import random
from django.utils import timezone
from django.urls import reverse
from django.views import generic, View
from django.core.exceptions import ObjectDoesNotExist

from rest_framework import viewsets
from .serializers import ScrumyUsersSerializer, ScrumyGoalsSerializer,  ScrumyStatusSerializer
from rest_framework import viewsets


	
def FrontendRenderView(request):
	return render_to_response('myscrumy/trial.html',{'request':request})


class ScrumyUsersViewSet(viewsets.ModelViewSet):
	queryset = ScrumyUser.objects.all()
	serializer_class = ScrumyUsersSerializer


class ScrumyGoalsViewSet(viewsets.ModelViewSet):
	queryset = ScrumyGoals.objects.all()
	serializer_class = ScrumyGoalsSerializer


class ScrumyStatusViewSet(viewsets.ModelViewSet):
	queryset = Status.objects.all()
	serializer_class = ScrumyStatusSerializer




@login_required
def index(request):

	users = ScrumyUser.objects.all()
	
	w = [] # Stores all the weekly task of a user
	d = [] # Stores all the daily task of a user
	v = [] # Stores all the task of a user under verify
	dn = [] # Stores all the task user has done.
	deleted = [] #Stores all the users task that has been deleted.
	
	for x in range(1, users.count()+1):
		try:
			y = ScrumyUser.objects.get(id=x)
		except ObjectDoesNotExist:
			pass

		k = y.scrumygoals_set.all()

		for eachtask in k:
			if eachtask.status_id_id ==4:
				w.append(eachtask)
			elif eachtask.status_id_id ==3:
				d.append(eachtask)
			elif eachtask.status_id_id ==2:
				v.append(eachtask)
			elif eachtask.status_id_id == 1:
				dn.append(eachtask)
			else:
				deleted.append(eachtask)


	return render(request, 'myscrumy/home.html',{'w':w,'d':d,'v':v,'dn':dn,'u':users})





	
def landing_view(request):

	if request.method == "POST":

		company_name = request.POST.get('companyname')
		email = request.POST.get('email')
		first_name = request.POST.get('firstname')
		last_name = request.POST.get('lastname')
		password = request.POST.get('password')

		manager = User.objects.create_user(company_name, email, password)
		manager.first_name = first_name
		manager.last_name = last_name
		

		user_group = Group.objects.get(name='Admin')
		user_group.user_set.add(manager)

		manager.save()

		return render (request, 'myscrumy/landing.html')
	else:
		return render(request, "myscrumy/landing.html")





@login_required
def add_user(request):
	if request.method == "POST":
		form = AddUserForm(request.POST)
		z = request.POST.get('userName')
		if form.is_valid():
			try:
				x = User.objects.get(username=z)
				if ScrumyUser.objects.filter(userName=z).count():
					return HttpResponse("user already exist"+"<br>"+"<a href='add_user'>Try Again</a>")
				else:
					user = form.save(commit=False)
					user.save()
					return HttpResponseRedirect('index')
			except ObjectDoesNotExist:
				return HttpResponse("Username does not exist"+"<br>"+"<a href='add_user'>Try Again</a>")
	else:
		form = AddUserForm()
		return render(request, 'myscrumy/add_user.html', {'form': form} )





@login_required
def add_task(request):

	if request.method == "POST":
		form = TaskPostForm(request.POST)
		if form.is_valid():
			taskpost = form.save(commit=False)

			tc = random.randint(1000,9999)
			t = ScrumyGoals.objects.filter(task_id = tc)
			if t.count():
				tc = random.randint(1000,9999)
			else:
				taskpost.task_id = tc

			a = Status.objects.get(pk = 4)
			taskpost.status_id = a

			taskpost.time_of_status_change = timezone.now()

			taskpost.created_by = request.user

			taskpost.save()

			task = ScrumyGoals.objects.get(task_id=tc)

			history = ScrumyHistory(created_by = request.user.username, task_id = task)
			history.save()
			return HttpResponseRedirect('index')
	else:
		form = TaskPostForm()
	return render(request, 'myscrumy/add_task.html', {'form': form} ) 





@login_required
def history(request,task_pk):
	task = ScrumyGoals.objects.get(id = task_pk)

	history = task.scrumyhistory_set.all()


	return render(request, 'myscrumy/history.html', {'history': history} )





@login_required
def move_task(request,task_id):
	
	if request.method == "POST":
		
		current_user = request.user  #Gets the currently logged in user
		current_user_group = Group.objects.get(user = current_user) #Gets the group of the current user from the Group table
		task = ScrumyGoals.objects.get(task_id = task_id ) #search database for record with userid and taskid = values supplied in from
		previous_status = task.status_id

		if task.owner == request.user.username:
			if request.POST.get("status") == 'weekly target':
				task.status_id_id = 4
				task.time_of_status_change = timezone.now()
				task.moved_by = request.user.username
				task.save()
				history = ScrumyHistory(moved_by = request.user.username, moved_from = previous_status, moved_to = "Weekly Target",time_of_status_change=timezone.now(),task_id=task)
				history.save()
				return HttpResponseRedirect(reverse('index'))
			elif request.POST.get("status") == 'daily target':
				task.status_id_id = 3
				task.time_of_status_change = timezone.now()
				task.moved_by = request.user.username
				task.save()
				history = ScrumyHistory(moved_by = request.user.username, moved_from = previous_status, moved_to = "Daily Target",time_of_status_change=timezone.now(),task_id=task)
				history.save()
				return HttpResponseRedirect(reverse('index'))
			elif request.POST.get("status") == 'verify':
				task.status_id_id = 2
				task.time_of_status_change = timezone.now()
				task.moved_by = request.user.username
				task.save()
				history = ScrumyHistory(moved_by = request.user.username, moved_from = previous_status, moved_to = "Verify",time_of_status_change=timezone.now(),task_id=task)
				history.save()
				return HttpResponseRedirect(reverse('index'))
			elif request.POST.get("status") == 'done':
				task.status_id_id = 1
				task.time_of_status_change = timezone.now()
				task.moved_by = request.user.username
				task.save()
				history = ScrumyHistory(moved_by = request.user.username, moved_from = previous_status, moved_to = "Done",time_of_status_change=timezone.now(),task_id=task)
				history.save()
				return HttpResponseRedirect(reverse('index'))
		elif current_user_group.name == 'Admin':
			if request.POST.get("status") == 'weekly target':
				task.status_id_id = 4
				task.time_of_status_change = timezone.now()
				task.moved_by = request.user.username
				task.save()
				history = ScrumyHistory(moved_by = request.user.username, moved_from = previous_status, moved_to = "Weekly Target",time_of_status_change=timezone.now(),task_id=task)
				history.save()
				return HttpResponseRedirect(reverse('index'))
			elif request.POST.get("status") == 'daily target':
				task.status_id_id = 3
				task.time_of_status_change = timezone.now()
				task.moved_by = request.user.username
				task.save()
				history = ScrumyHistory(moved_by = request.user.username, moved_from = previous_status, moved_to = "Daily Target",time_of_status_change=timezone.now(),task_id=task)
				history.save()
				return HttpResponseRedirect(reverse('index'))
			elif request.POST.get("status") == 'verify':
				task.status_id_id = 2
				task.time_of_status_change = timezone.now()
				task.moved_by = request.user.username
				task.save()
				history = ScrumyHistory(moved_by = request.user.username, moved_from = previous_status, moved_to = "Verify",time_of_status_change=timezone.now(),task_id=task)
				history.save()
				return HttpResponseRedirect(reverse('index'))
			else:
				return HttpResponseRedirect(reverse('no_permission'))
		elif current_user_group.name == 'Developer':
			if request.POST.get("status") == 'weekly target':
				task.status_id_id = 4
				task.time_of_status_change = timezone.now()
				task.moved_by = request.user.username
				task.save()
				history = ScrumyHistory(moved_by = request.user.username, moved_from = previous_status, moved_to = "Weekly Target",time_of_status_change=timezone.now(),task_id=task)
				history.save()
				return HttpResponseRedirect(reverse('index'))
			elif request.POST.get("status") == 'daily target':
				task.status_id_id = 3
				task.time_of_status_change = timezone.now()
				task.moved_by = request.user.username
				task.save()
				history = ScrumyHistory(moved_by = request.user.username, moved_from = previous_status, moved_to = "Daily Target",time_of_status_change=timezone.now(),task_id=task)
				history.save()
				return HttpResponseRedirect(reverse('index'))
			else:
				return HttpResponseRedirect(reverse('no_permission'))
		elif current_user_group.name == 'QualityAssurance':
			if request.POST.get("status") == 'done':
				task.status_id_id = 1
				task.time_of_status_change = timezone.now()
				task.moved_by = request.user.username
				task.save()
				history = ScrumyHistory(moved_by = request.user.username, moved_from = previous_status, moved_to = "Done",time_of_status_change=timezone.now(),task_id=task)
				history.save()
				return HttpResponseRedirect(reverse('index')) 
			else:
				return HttpResponseRedirect(reverse('no_permission'))
		else:
			return HttpResponseRedirect(reverse('no_permission'))
			
		
		'''if request.POST.get("status") == 'weekly target':
			task.status_id_id = 4
			task.time_of_status_change = timezone.now()
			task.moved_by = request.user.username
			task.movement_track = previous_status
			task.save()
			return HttpResponseRedirect(reverse('index'))
		elif request.POST.get("status") == 'daily target':
			task.status_id_id = 3
			task.time_of_status_change = timezone.now()
			task.moved_by = request.user.username
			task.movement_track =  previous_status
			task.save()
			return HttpResponseRedirect(reverse('index'))
		elif request.POST.get("status") == 'verify':
			Regular = Group.objects.get(name = 'Regular')
			if current_user_group.name == 'Regular':
				return HttpResponseRedirect(reverse('no_permission'))
			else:
				task.status_id_id = 2
				task.time_of_status_change = timezone.now()
				task.moved_by = request.user.username
				task.movement_track = previous_status
				task.save()
				return HttpResponseRedirect(reverse('index'))
		elif request.POST.get("status") == 'done':
			Regular = Group.objects.get(name = 'Regular')
			if current_user_group.name == 'Regular':
				return HttpResponseRedirect(reverse('no_permission'))
			else:
				task.status_id_id = 1
				task.time_of_status_change = timezone.now()
				task.moved_by = request.user.username
				task.movement_track = previous_status
				task.save()
				return HttpResponseRedirect(reverse('index'))
		else:
			task.status_id_id = 5'''
	else:
		return render(request, 'myscrumy/move_task.html')





@login_required
def no_permission(request):
	return render(request, "myscrumy/not_permited.html")





@login_required
def delete_task(request,task_id):

	if request.method == "POST":

		if request.POST.get("delete") == 'yes':
			task_to_delete = ScrumyGoals.objects.get(task_id=task_id)
			task_to_delete.status_id_id = 5
			task_to_delete.save()
			return HttpResponseRedirect(reverse('index'))
		else:
			return HttpResponseRedirect(reverse('index'))	
	else:
		return render(request, "myscrumy/confirm_delete.html")


@login_required
def assign_task_owner(request,task_id):

	if request.method == "POST":
		form_data = request.POST.get('userName') #get username submitted by user
		if ScrumyUser.objects.filter(userName = form_data).count():
			task = ScrumyGoals.objects.get(task_id=task_id)
			task.owner = form_data
			task.save()
			return HttpResponseRedirect(reverse('index'))
		else:
			return HttpResponse("Username does not exist"+"<br>"+"<a href='/index'>Try Again</a>")
	else:
		form = AssignTaskOwnerForm()
	return render(request, 'myscrumy/assign_task_owner.html', {'form': form} )


