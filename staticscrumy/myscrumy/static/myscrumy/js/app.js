var app = angular.module('dynamicScrumyApp', ['ngRoute', 'dynamicScrumyController']);



app.config(['$routeProvider',
	function($routeProvider){
		$routeProvider
		.when('/add_task', {
				template: '<form ng-controller="dynamicScrumyController">Task:<br><br><textarea></textarea><br><input ng-click="addNewTask()" type="submit" value="Add Task"></form>',
				controller: 'dynamicScrumyController'
			});
		$routeProvider
		.when('/add_user', {
			template: 'Username:<br><input ng-model="username"><br><input type="submit" value="Add User" ng-click="addNewUser()">',
			controller: 'dynamicScrumyController'
		})
	}
]);


app.config([
	'$httpProvider', function($httpProvider){
		$httpProvider.defaults.xsrfCookieName = 'csrftoken';
		$httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
	}
])


