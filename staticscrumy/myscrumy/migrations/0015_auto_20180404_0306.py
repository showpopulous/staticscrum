# Generated by Django 2.0.1 on 2018-04-04 02:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('myscrumy', '0014_scrumyhistory'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='scrumygoals',
            name='movement_track',
        ),
        migrations.AlterField(
            model_name='scrumyhistory',
            name='moved_from',
            field=models.CharField(default='nill', max_length=50),
        ),
    ]
